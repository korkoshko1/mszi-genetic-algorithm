<?php

namespace korkoshko;

class Population
{
    /**
     * @var array
     */
    protected array $individuals = [];

    /**
     * @param int $numOfIndividual
     *
     * @return Population
     */
    public function create(int $numOfIndividual): self
    {
        $this->individuals = array_fill(0, $numOfIndividual, null);
        $this->individuals = array_map(fn($item) => new Individual, $this->individuals);
        $this->sort();

        return $this;
    }

    /**
     * @param array $individuals
     *
     * @return $this
     */
    public function add(array $individuals): self
    {
        foreach ($individuals as $individual) {
            $fitnessInd = $this->getFitnessInd();

            if ($individual->getNumber() > $fitnessInd->getNumber()) {
                array_unshift($this->individuals, $individual);
            } else {
                array_push($this->individuals, $individual);
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function next(): self
    {
        $allNumbers = array_map(fn($individual) => $individual->getNumber(),
            $this->individuals
        );

        $this->individuals = array_filter($this->individuals,
            fn($individual) => in_array($individual->getNumber(), $allNumbers)
        );

        return $this;
    }

    /**
     * @return array|null
     */
    public function getIndividuals(): ?array
    {
        return $this->individuals;
    }

    /**
     * @return Individual
     */
    public function getFitnessInd(): ?Individual
    {
        return $this->individuals[0];
    }

    /**
     * @return bool
     */
    public function sort(): bool
    {
        return usort($this->individuals,
            fn($current, $next) => ($next->getNumber() - $current->getNumber()) % 2
        );
    }
}