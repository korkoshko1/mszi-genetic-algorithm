<?php

namespace korkoshko;

class GeneticAlgorithm
{
    /**
     * @var Population
     */
    protected Population $population;

    /**
     * @var Individual
     */
    protected ?Individual $prevFitnessIndOfPopulation = null;

    /**
     * @var int
     */
    protected int $criteria;

    /**
     * @var int
     */
    protected int $criteriaCounter = 0;

    /**
     * @var callable
     */
    protected $iterationCallback;

    /**
     * GeneticAlgorithm constructor.
     *
     * @param int $numOfPopulation
     * @param int $criteria
     */
    public function __construct(int $numOfPopulation, int $criteria = 80)
    {
        $this->population = (new Population)->create($numOfPopulation);
        $this->criteria = $criteria;
    }

    /**
     * @return Individual|null
     * @throws \Exception
     */
    public function run(): ?Individual
    {
        $iteration = 0;

        while ($this->isNotReachedOptimal() && ++$iteration) {
            $this->population->next();
            $this->crossover();

            if (is_callable($this->iterationCallback)) {
                call_user_func($this->iterationCallback, $iteration, $this->population);
            }
        }

        return $this->prevFitnessIndOfPopulation;
    }

    /**
     * @param callable $callback
     *
     * @return GeneticAlgorithm
     */
    public function setIterationCallback(callable $callback): self
    {
        $this->iterationCallback = $callback;
        return $this;
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function parents(): array
    {
        $individuals = $this->population->getIndividuals();
        $individualsLength = count($individuals) - 1;

        $fixDuplicateParents = function (array $individuals, int $index) {
            unset($individuals[$index]);
            return array_values($individuals);
        };

        $firstParent = $individuals[$randomIndividual = random_int(0, $individualsLength)];
        $individuals = $fixDuplicateParents($individuals, $randomIndividual);

        $previousSimilar = Individual::MAX_BITS;
        $randomIndividual = 0;

        foreach ($individuals as $index => $individual) {
            $similar = similar_text($firstParent->getBinary(), $individual->getBinary());

            if ($similar < $previousSimilar) {
                $previousSimilar = $similar;
                $randomIndividual = $index;
            }
        }

        $secondParent = $individuals[$randomIndividual];
        $individuals = $fixDuplicateParents($individuals, $randomIndividual);

        $maskParent = clone $individuals[random_int(0, $individualsLength - 2)];

        return [
            $firstParent,
            $secondParent,
            $maskParent->mutate(10),
        ];
    }

    /**
     * @throws \Exception
     */
    private function crossover(): void
    {
        $parents = array_fill(0, 8, null);

        foreach ($parents as $parent) {
            $parent = $this->parents();

            $this->population->add(
                $this->triadCrossover($parent)
            );
        }
    }

    /**
     * @param array $parent
     *
     * @return array
     * @throws \Exception
     */
    private function triadCrossover(array $parent)
    {
        $firstChild = '';
        $secondChild = '';

        $binary = array_map(fn($individual) => $individual->getBinary(), $parent);

        for ($i = 0; $i < Individual::MAX_BITS; $i++) {
            $index = (int) ($binary[0][$i] !== $binary[2][$i]);

            $firstChild .= $binary[$index][$i];
            $secondChild .= $binary[!$index][$i];
        }

        return [
            (new Individual($firstChild))->mutate(3),
            (new Individual($secondChild))->mutate(3),
        ];
    }

    /**
     * @return bool
     */
    private function isNotReachedOptimal()
    {
        $individual = $this->population->getFitnessInd();
        $isNotOK = !$this->prevFitnessIndOfPopulation ||
            $individual->getNumber() !== ($this->prevFitnessIndOfPopulation->getNumber());

        if ($isNotOK) {
            $this->criteriaCounter = 0;
            $this->prevFitnessIndOfPopulation = $individual;
            return true;
        }

        return ++$this->criteriaCounter < $this->criteria;
    }
}