<?php

require_once __DIR__ . '/../vendor/autoload.php';

use korkoshko\{
    GeneticAlgorithm,
    Population
};

$iterationOfPopulation = function (int $iteration, Population $population) {
    $fitnessIndividual = $population->getFitnessInd();

    echo 'Iteration: '
        . $iteration . ' | '
        . $fitnessIndividual->getNumber() . ' | '
        . $fitnessIndividual->getBinary()
        . PHP_EOL;
};

$fitnessIndividual = (new GeneticAlgorithm(20))
    ->setIterationCallback($iterationOfPopulation)
    ->run();

echo PHP_EOL . 'Final: '
    . $fitnessIndividual->getNumber() . ' | '
    . $fitnessIndividual->getBinary()
    . PHP_EOL;